const firstNum = 8 ** 2;
console.log("Using ** Operator")
console.log(firstNum);

const secondNum = Math.pow(8, 2);
console.log("Using Math.pow()");
console.log(secondNum);

let name = "John";

let message = 'Hello ' + name + '! Welcome to programming!';
console.log("Message without template literals: " + message);
message = `Hello ${name}! Welcome to programming`;
console.log(`Message with template literals: ${message}`);

const anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${firstNum}.
`

console.log(anotherMessage);

const interestRate = 0.1;
const principal = 1000;

console.log(`The interest on your savings account is: ${principal * interestRate}`);

const fullName = ['John', 'Peters', 'Smith'];

console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`)

const [firtName, middleName, lastName] = fullName;
console.log(`Hello ${firtName} ${middleName} ${lastName}! It's nice to meet you.`);

const nation = ['USA', 'California'];
const [country, state] = nation;
console.log(`Welcome to ${state}, ${country}`);

const person = {
	givenName: "Jane",
	maidenName: "Doe",
	familyName: "Smith"
}

console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`);

const {givenName, maidenName, familyName} = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again.`);

function getFullName ({givenName, maidenName, familyName}){
	console.log(`${givenName} ${maidenName} ${familyName}`);
}

getFullName(person);

const hello = () => {
	console.log("Hello, world!")
}
hello();

const printFullName = (firstName, middleinitial, lastName) => {
	console.log(`${firstName} ${middleinitial}. ${lastName}`);
}

printFullName("John", "D", "Smith");

const students = ['John', 'Jane', 'Judy'];

console.log('Using traditional function');

students.forEach(function(student){
	console.log(`${student} is a student.`);
})

console.log('Using arrow function');

students.forEach((student) => {
	console.log(`${student} is a student.`);

console.log("Using traditional function");
function add(x, y){
	return x + y;
}

let total = add(1, 2);
console.log(total);

// Arrow function
console.log('Using arrow function');
const addNew = (x, y) => x + y;
let totalNew = addNew(1, 3);
console.log(totalNew);

const greet = (name = 'User') => {
	return `Good morning, ${name}`;
}

console.log(greet());
console.log(greet('Aizaac'));